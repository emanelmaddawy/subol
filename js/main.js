/* global src */

$(document).ready(function () {


new WOW().init();

$('.owl-carousel#sync1').owlCarousel({
  items:1,
  loop:true,
  nav: true,
  navText: ['<i class="fa fa-angle-left"><i>', '<i class="fa fa-angle-right"><i>'],
  URLhashListener:true,
  autoplayHoverPause:true,
  startPosition: 'URLHash'
});

$('.owl-carousel#sync2').owlCarousel({
  items:5,
  loop:true,
  nav: true,
  navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
  autoplayHoverPause:true,
  startPosition: 'URLHash',
  responsive:{
      0:{
          items:1,
      },
      600:{
          items:3,
          mouseDrag:true
      },
      1000:{
          items:5
      }
  }
});


});
